---
title: "Setting up visual studio code so it works well with a screenreader"
date: 2018-12-26T00:29:41-06:00
tags: [visual studio code, shortcuts]
---
# Downloading
first, download [visual studio code](https://code.visualstudio.com/Download#).
# installing
Simply run the setup file that you just downloaded.
# setting up accessibility
Before you can use visual  studio code you have to adjust some settings.
* open visual studio code.
* The first time you open visual studio code, you will see a welcome window.
* To not see this window everytime, find the checkbox that is labeled "Show welcome page on startup" and check or uncheck it.
* to open up the settings, press ctrl + ,.
* type "accessibility" in the search box and arrow down.
* You'll land on the enable accessibility in editor setting, set this to on.
{{< warning >}}
If you enable accessibility for the editor, word wrap will be disabled.
{{< /warning >}}

# tips in editor
To access suggestions simply arrow through once the list pops up. You will get alerted about this if it occurs
To confirm a suggestion press tab or enter.
{{< warning >}}
Line numbers are not red out at the moment. To see line numbers, press ctrl + G in the editor to hear current line number in focus.
{{< /warning >}}