---
title: "Using command line"
date: 2020-08-04T21:05:05-05:00
tags: [command line, shortcuts]
---

Working with the command line from a sighted perspective looks simple. But as a visually impaired person you will encounter some dificulties.
To view output using a screenreader:
* pull up a command line window.
* press windows + R and type "cmd" and press enter.
* type a command.
Now you will likely see output on the screen, to go up in the output there are two ways. Using the screen reader command or a windows shortcut.
Note: using this method will likely not scroll up to the top completely. The windows method is recommended.
* For the screenreader NVDA use the NVDA key (I prefer capslock) and the arrow keys.
* for the windows shortcut press ctrl + M, and use the arrows normally. To get back, press escape.

