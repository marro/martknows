---
title: "using git"
date: 2018-12-26T00:29:41-06:00
tags: [git, shortcuts]
---
This page is about using git when you are completely blind. I will explain how it went for me from creating an account to setting up control with the command line.
# Creating an account
Creating a gitlab account is pretty straight forward. Go to the [GitLab site](https://gitlab.com/).
## Managing
There are two ways to manage your repositories: SSH and the web interface.
I'll only be covering the SSH method here, since it is easier for a blind person to use the command line then the web interface. You can use the web interface as a blind person but it is not the best experience. The web interface could definitely use some improvements.
## Setting up SSH
First we have to navigate to the settings page of your gitlab account.
* Log into your gitlab account.
* Click on your name in the menubar and click on settings in the menu that expands.
* Navigate to the SSH tab.
* open a command window, press windows + R and type cmd and press enter.
* paste the following command:
```
ssh-keygen -t rsa -b 2048 -C "<comment>"
```
* follow the instructions on screen.
* if done correctly, the key will appear in the following directory: C:\Users\current user\.ssh
The file is stored in a file called id_rsa.pub. This is following the defaults when generating.
* After generating your key, you go back to the web page where you put the SSH key.
* copy all the contents in the file where your key is stored. Paste everything in the key field, give your key a proper title and save it.

# Commands to remember
cloning a repository is as follows:
* on the git site, go to the project you want to clone.
* Click on the code button and copy the ssh value, looks something like this:
```
git@gitlab.com:user/projectname.git
```
* for pushing do the following:
```
git commit -a -m "message"
git push
```
* if you want to check what is being pushed use git status after commiting.
* If a project has a submodule that does not download with the cloning use following command:
```
git submodule update --init --recursive
```
* if you want  to overwrite changes and fetch the latest commit on git use the following commands:
```
git fetch
git reset --hard HEAD
git merge @{u}
```

