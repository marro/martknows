---
date: "2021-03-16"
title: "About"
---

Mart Knows is a website dedicated to visual impaired coders.


Learn more and contribute on [GitLab](https://marro/gitlab.com/martknows).
