---
date: "2017-06-26T18:27:58+01:00"
title: "Home"
---

Welcome to **Mart Knows**: 

This is an accessibility-friendly site about how to write code as visual impaired. This is also the place where I'll note things I learn along the way:

{{< ticks >}}
* Screen reader and keyboard accessible tools
* IDE configurations 
* Tips and tricks
* Other resources
{{< /ticks >}}
#Contact
{{< ticks >}}
* Email: martlohuis@live.nl
* twitter: @marro_nl

{{< /ticks >}}